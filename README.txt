This is a super basic module. Help welcome!

Maybe you want to use CloudFlare.

Maybe you want to export your settings and store them in revision control so
you can see if/when it has changed.

Maybe you like using Drush.

Great.

You should put your email and API into your settings.php file like so:

  $conf['chotchkie_email'] = 'you@example.com';
  $conf['chotchkie_token'] = 'supersecrettoken';

To install, you should be using something like composer_manager to get the
composer.json for this module included into your sitewide composer files.
