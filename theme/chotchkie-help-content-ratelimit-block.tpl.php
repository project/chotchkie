<p><?php print t('Access denied.'); ?></p>
<h2><?php print t('What happened?'); ?></h2>
<p><?php print t('The owner of this website has banned you temporarily from accessing @site_name.'); ?></p>
<p><?php print t('If you believe this happened in error, please contact the site maintainer.'); ?></p>
