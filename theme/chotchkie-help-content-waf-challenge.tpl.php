  <p><?php print t('Please complete the security check to access @site_name.', array('@site_name' => variable_get('site_name', 'Drupal'))); ?></p>
  <p>::CAPTCHA_BOX::</p>
<h2><?php print t('Why do I have to complete a CAPTCHA?'); ?></h2>
<p><?php print t('Completing the CAPTCHA proves you are a human and gives you temporary access to @site_name.', array('@site_name' => variable_get('site_name', 'Drupal'))); ?></p>
<h2><?php print t('What can I do to prevent this in the future?'); ?></h2>
<p><?php print t('If you are on a personal connection, like at home, you can run an anti-virus scan on your device to make sure it is not infected with malware.'); ?></p>
<p><?php print t('If you are at an office or shared network, you can ask the network administrator to run a scan across the network looking for misconfigured or infected devices.'); ?></p>
