<p><?php print t('Access denied.'); ?></p>
<h2><?php print t('What happened?'); ?></h2>
<p><?php print t('The owner of this website has banned your IP address: ::CLIENT_IP::.'); ?></p>
<p><?php print t('If you believe this happened in error, please contact the site maintainer and reference your IP and the Ray ID: ::RAY_ID::
'); ?></p>
