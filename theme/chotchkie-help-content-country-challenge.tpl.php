  <p><?php print t('Please complete the security check to access this site.'); ?></p>
  <p>::CAPTCHA_BOX::</p>
<h2><?php print t('Why do I have to complete a CAPTCHA?'); ?></h2>
  <p><?php print t('Completing the CAPTCHA proves you are a human and gives you temporary access to @site_name.', array('@site_name' => variable_get('site_name', 'Drupal'))); ?></p>
  <p><?php print t('Our site has detected your IP address as ::CLIENT_IP:: and believes you are in ::GEO::. If you contact support about this issue you can mention your Ray ID for this request is ::RAY_ID::'); ?></p>
