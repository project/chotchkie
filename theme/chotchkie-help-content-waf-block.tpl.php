  <p><?php print t('Your requests are blocked.'); ?></p>
<h2><?php print t('Why have I been blocked?'); ?></h2>
  <p><?php print t('This website is using a security service to protect itself from online attacks. The action you just performed triggered the security solution. There are several actions that could trigger this block including submitting a certain word or phrase, a SQL command or malformed data.'); ?></p>
<h2><?php print t('What can I do to resolve this?'); ?></h2>
  <p><?php print t('If you believe this happened in error, please contact the site maintainer and let them know the time of the error message and your IP address.'); ?></p>
