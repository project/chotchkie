  <p>::ALWAYS_ONLINE_NO_COPY_BOX::</p>
<h2><?php print t('What happened?'); ?></h2>
  <p><?php print t('The website is not currently responding to requests.'); ?></p>
<h2><?php print t('What can I do to resolve this?'); ?></h2>
  <p><?php print t('Please try again in a few minutes.'); ?></p>
