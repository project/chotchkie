<?php

/**
 * @file
 * Module file for Chotchkie module.
 */

/**
 * Implements hook_menu().
 */
function chotchkie_menu() {
  // This default value is not special. You should set a different one.
  $prefix = variable_get('chotchkie_path_prefix', 'ajR_ged39YmHmq9xeCuF0GtlQpLKLjvtqJSoyXQ8geM');

  return array(
    $prefix . '/chotchkie/%' => array(
      'page callback' => 'chotchkie_custom_page',
      // Access is open on purpose so Cloudflare can get to these pages.
      'access callback' => TRUE,
      'type' => MENU_CALLBACK,
      'page arguments' => array(2),
    ),
  );
}

/**
 * Helper function to ensure the path prefix exists.
 */
function chotchkie_ensure_prefix_exists() {
  if (empty(variable_get('chotchkie_path_prefix'))) {
    variable_set('chotchkie_path_prefix', drupal_random_key());
  }
}

/**
 * Implements hook_init().
 */
function chotchkie_init() {
  $custom_pages = chotchkie_cloudflare_custom_pages();
  $page_name = arg(2);
  if (array_key_exists($page_name, $custom_pages)) {
    global $conf;
    $conf['maintenance_mode'] = 1;
    $template = drupal_get_path('module', 'chotchkie') . '/theme/' . 'chotchkie-help-content-' . str_replace('_', '-', $page_name) . '.tpl.php';
    $conf['maintenance_mode_message'] = theme_render_template($template, array());
  }
}

/**
 * Overrides theme_preprocess_maintenance_page().
 */
function chotchkie_preprocess_maintenance_page(&$variables) {
  $custom_pages = chotchkie_cloudflare_custom_pages();
  $page_name = arg(2);
  if (array_key_exists($page_name, $custom_pages)) {
    // The page title cannot be set in the page callback on a maintenance page.
    // So...we have to do it here.
    // The rest of this is copy-paste from theme_preprocess_maintenance_page().
    drupal_set_title($custom_pages[$page_name]);
    // Cloudflare uses Collapsify to get pages and Collapsify requires a 200.
    drupal_add_http_header('Status', '200 OK');
  }

  // Add favicon.
  if (theme_get_setting('toggle_favicon')) {
    $favicon = theme_get_setting('favicon');
    $type = theme_get_setting('favicon_mimetype');
    drupal_add_html_head_link(array('rel' => 'shortcut icon', 'href' => drupal_strip_dangerous_protocols($favicon), 'type' => $type));
  }

  global $theme;
  // Retrieve the theme data to list all available regions.
  $theme_data = list_themes();
  $regions = $theme_data[$theme]->info['regions'];

  // Get all region content set with drupal_add_region_content().
  foreach (array_keys($regions) as $region) {
    // Assign region to a region variable.
    $region_content = drupal_get_region_content($region);
    isset($variables[$region]) ? $variables[$region] .= $region_content : $variables[$region] = $region_content;
  }

  // Setup layout variable.
  $variables['layout'] = 'none';
  if (!empty($variables['sidebar_first'])) {
    $variables['layout'] = 'first';
  }
  if (!empty($variables['sidebar_second'])) {
    $variables['layout'] = ($variables['layout'] == 'first') ? 'both' : 'second';
  }

  // Construct page title
  if (drupal_get_title()) {
    $head_title = array(
      'title' => strip_tags(drupal_get_title()),
      'name' => variable_get('site_name', 'Drupal'),
    );
  }
  else {
    $head_title = array('name' => variable_get('site_name', 'Drupal'));
    if (variable_get('site_slogan', '')) {
      $head_title['slogan'] = variable_get('site_slogan', '');
    }
  }

  // set the default language if necessary
  $language = isset($GLOBALS['language']) ? $GLOBALS['language'] : language_default();

  $variables['head_title_array']  = $head_title;
  $variables['head_title']        = implode(' | ', $head_title);
  $variables['base_path']         = base_path();
  $variables['front_page']        = url();
  $variables['breadcrumb']        = '';
  $variables['feed_icons']        = '';
  $variables['help']              = '';
  $variables['language']          = $language;
  $variables['language']->dir     = $language->direction ? 'rtl' : 'ltr';
  $variables['logo']              = theme_get_setting('logo');
  $variables['messages']          = $variables['show_messages'] ? theme('status_messages') : '';
  $variables['main_menu']         = array();
  $variables['secondary_menu']    = array();
  $variables['site_name']         = (theme_get_setting('toggle_name') ? variable_get('site_name', 'Drupal') : '');
  $variables['site_slogan']       = (theme_get_setting('toggle_slogan') ? variable_get('site_slogan', '') : '');
  $variables['tabs']              = '';
  $variables['title']             = drupal_get_title();

  // Compile a list of classes that are going to be applied to the body element.
  $variables['classes_array'][] = 'in-maintenance';
  if (isset($variables['db_is_active']) && !$variables['db_is_active']) {
    $variables['classes_array'][] = 'db-offline';
  }
  if ($variables['layout'] == 'both') {
    $variables['classes_array'][] = 'two-sidebars';
  }
  elseif ($variables['layout'] == 'none') {
    $variables['classes_array'][] = 'no-sidebars';
  }
  else {
    $variables['classes_array'][] = 'one-sidebar sidebar-' . $variables['layout'];
  }

  // Dead databases will show error messages so supplying this template will
  // allow themers to override the page and the content completely.
  if (isset($variables['db_is_active']) && !$variables['db_is_active']) {
    $variables['theme_hook_suggestion'] = 'maintenance_page__offline';
  }
}

/**
 * Page callback for Cloudflare custom error pages.
 *
 * @param string $page_name
 *   A valid Cloudflare custom error page identifier.
 *
 * @return array|int
 *   A render array or 404 if the request is invalid.
 */
function chotchkie_custom_page($page_name) {
  $build = array();
  $custom_pages = chotchkie_cloudflare_custom_pages();
  if (array_key_exists($page_name, $custom_pages)) {
    $build['chotchkie_custom_page_key'] = $page_name;
    return $build;
  }
  return MENU_NOT_FOUND;
}

/**
 * List of known custom pages and the required tokens.
 *
 * @return array
 *   An array of page ID values and titles to use.
 */
function chotchkie_cloudflare_custom_pages() {
  return array(
    'basic_challenge' => 'One more step',
    'waf_challenge' => 'One more step',
    'country_challenge' => 'One more step',
    'waf_block' => 'You have been blocked',
    'ratelimit_block' => 'You are being rate limited',
    'ip_block' => 'Your IP is blocked',
    'under_attack' => 'Checking your browser...',
    '500_errors' => 'Error - 500',
    '1000_errors' => 'Error - 1000',
    'always_online' => 'Website is offline',
  );
}

/**
 * Implements hook_theme().
 */
function chotchkie_theme() {
  $pages = chotchkie_cloudflare_custom_pages();
  foreach ($pages as $page_key => $page_title) {
    $items[] =
      array(
        'chotchkie_help_content_' . $page_key => array(
          'template' => 'chotchkie-help-content-' . str_replace('_', '-', $page_key),
          'path' => drupal_get_path('module', 'chotchkie') . '/theme',
        ),
      );
  }
  return $items;
}
