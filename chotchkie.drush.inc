<?php

/**
 * @file
 * Drush commands for Chotchkie module.
 */

use Cloudflare\Api;
use Cloudflare\Zone;
use Cloudflare\Zone\Settings;
use Cloudflare\Zone\CustomPages;
use Cloudflare\Zone\Firewall\AccessRules
use Cloudflare\Zone\Pagerules;
use Cloudflare\Zone\WAF\Packages;
use Cloudflare\Zone\WAF\Packages\Rules;

/**
 * Implements hook_drush_command().
 */
function chotchkie_drush_command() {
  return [
    'chotchkie-get-settings' => [
      'description' => "Get settings from Cloudflare.",
      'options' => [
        'zone-name' => dt('Limits the output to focus on a zone name (usually the domain name).'),
      ],
    ],
    'chotchkie-get-zones' => [
      'description' => "Get list of zones from Cloudflare. Run this before getting settings so you know what --zone-name to pass to the settings.",
    ],
    'chotchkie-list-custom-pages'  => [
      'description' => "Get list of possible custom pages from Cloudflare and print links to the urls on your site.",
      'options' => [
        'zone-name' => dt('Limits the output to focus on a zone name (usually the domain name).'),
      ],
    ],
  ];
}

/**
 * Drush command callback for `chotchkie-get-zones`.
 */
function drush_chotchkie_get_zones() {
  $client = new Api(variable_get('chotchkie_email'), variable_get('chotchkie_token'));

  $cf_zone = new Zone($client);
  $zones = $cf_zone->zones();
  foreach ($zones->result as $zone) {
    drush_log(dt("Zone name '@name' status '@status' with id '@id'", array(
      '@name' => $zone->name,
      '@status' => $zone->status,
      '@id' => $zone->id,
    )), 'success');
  }

}

/**
 * Drush command callback for `chotchkie-get-settings`.
 */
function drush_chotchkie_get_settings() {
  $output = '';

  // Create a connection to the Cloudflare API which you can
  // then pass into other services.
  $client = new Api(variable_get('chotchkie_email'), variable_get('chotchkie_token'));

  $cf_zone = new Zone($client);
  $zone_name = drush_get_option('zone-name', NULL);
  $zones = $cf_zone->zones($zone_name);

  if (empty($zone_name) && count($zones->result) > 1) {
    drush_log(dt('Your email and token have access to multiple zones in Cloudflare (multiple domains), but you did not specify a --zone-name. You should consider running drush chotchkie-get-zones to get a list of zones and re-running this command with --zone-name set to focus in on the settings for one zone.'), 'warning');
  }

  foreach ($zones->result as $zone) {
    drush_log(dt('Gathering and printing data for @zone_name', array('@zone_name' => $zone->name)), 'ok');

    // The "settings".
    $settings = new Settings($client);
    $all_settings = $settings->settings($zone->id);
    foreach ($all_settings->result as $setting) {
      $output .= dt("@id: !value.", array(
        '@id' => $setting->id,
        '!value' => json_encode($setting->value),
      )) . PHP_EOL;
    }

    // Zone Access Rules.
    $zone_access_rules = new AccessRules($client);
    foreach ($zone_access_rules->rules($zone->id)->result as $rule) {
      $output .= dt("Firewall Access Rule to '@mode' for target '@target:@value' status is '@status' notes '@notes' and scope: !scope",
        array(
          '@mode' => $rule->mode,
          '@target' => $rule->configuration->target,
          '@value' => $rule->configuration->value,
          '@status' => $rule->status,
          '@notes' => $rule->notes,
          '!scope' => json_encode($rule->scope),
        )) . PHP_EOL;
    }

    // Custom Pages.
    $pages = new CustomPages($client);
    foreach ($pages->custom_pages($zone->id)->result as $page) {
      $output .= dt("Custom page '@description' (@id) at url '@url' state is '@state'",
        array(
          '@description' => $page->description,
          '@id' => $page->id,
          '@url' => $page->url,
          '@state' => $page->state,
        )) . PHP_EOL;
    }
    // Pagerules (partially implemented).
    $page_rules = new Pagerules($client);
    if (count($page_rules->list_pagerules($zone->id)->result) == 0) {
      drush_log(dt("There are no page rules."), 'warning');
    }
    else {
      foreach ($page_rules->list_pagerules($zone->id)->result as $page_rule) {
        // TODO: clean this up.
        print_r($page_rule);
        drush_log('Page rules are not really supported yet. Please file a ticket with their structure.', 'warning');
      }
    }
  }

  // WAF Rules.
  $waf_packages = new Packages($client);
  if (count($waf_packages->rules($zone->id)->result) == 0) {
    drush_log(dt("There are no waf packages."), 'warning');
  }
  else {
    foreach ($waf_packages->rules($zone->id)->result as $waf_package) {
      $output .= dt("## WAF Rule Group '@name' (@id) description '@description' detection mode '@detection_mode' and action mode '@action_mode' and sensitivity '@sensitivity'.",
        array(
          '@name' => $waf_package->name,
          '@id' => $waf_package->id,
          '@description' => $waf_package->description,
          '@detection_mode' => $waf_package->detection_mode,
          '@action_mode' => isset($waf_package->action_mode) ? $waf_package->action_mode : '',
          '@sensitivity' => isset($waf_package->sensitivity) ? $waf_package->sensitivity : '',
        )) . PHP_EOL;

      $waf_rules = new Rules($client);
      if (count($waf_rules->rules($zone->id, $waf_package->id)->result) == 0) {
        drush_log(dt("There are no waf rules."), 'warning');
      }
      else {
        foreach ($waf_rules->rules($zone->id, $waf_package->id)->result as $page_rule) {
          $output .= dt("WAF Rule in group '@group' '@description' (@id) priority '@priority' is turned to mode '@mode'", array(
            '@group' => $page_rule->group->name,
            '@description' => $page_rule->description,
            '@id' => $page_rule->id,
            '@priority' => $page_rule->priority,
            '@mode' => $page_rule->mode,
          )) . PHP_EOL;
        }
      }
    }
  }

  print $output . PHP_EOL;

  // TODO: Firewall events
  // TODO: Railguns.
  // TODO: Accelerated Mobile Pages (Zone\Aml).
  // TODO: Argo.
  // TODO: Load Balancer.
}

/**
 * Drush callback for `chotchkie-list-custom-pages`.
 */
function drush_chotchkie_list_custom_pages() {
  // Create a connection to the Cloudflare API which you can
  // then pass into other services.
  $client = new Api(variable_get('chotchkie_email'), variable_get('chotchkie_token'));

  $cf_zone = new Zone($client);
  $zone_name = drush_get_option('zone-name', NULL);
  $zones = $cf_zone->zones($zone_name);

  if (empty($zone_name) && count($zones->result) > 1) {
    drush_log(dt('Your email and token have access to multiple zones in Cloudflare (multiple domains), but you did not specify a --zone-name. You should run drush chotchkie-get-zones to get a list of zones and re-run this command with --zone-name set to see custom pages for this one zone.'), 'warning');
    return;
  }

  foreach ($zones->result as $zone) {
    // Get a list of known local pages.
    $local_pages = chotchkie_cloudflare_custom_pages();

    $prefix = variable_get('chotchkie_path_prefix', 'ajR_ged39YmHmq9xeCuF0GtlQpLKLjvtqJSoyXQ8geM');

    // Get a list of Cloudflare known Custom Pages.
    $pages = new CustomPages($client);
    foreach ($pages->custom_pages($zone->id)->result as $page) {
      if (array_key_exists($page->id, $local_pages)) {
        print url($prefix . '/chotchkie/' . $page->id, array('absolute' => TRUE)) . PHP_EOL;
      }
      else {
        drush_log('Cloudflare supports a custom page for @description (id: @id). Please file an issue at https://www.drupal.org/project/issues/chotchkie?categories=All to add support', 'warning');
      }
    }
  }
}
